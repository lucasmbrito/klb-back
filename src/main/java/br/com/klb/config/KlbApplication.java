package br.com.klb.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({ "br.com.klb.*" })
@EntityScan("br.com.klb.*")
@EnableJpaRepositories("br.com.klb.repository")
public class KlbApplication {

	public static void main(String[] args) {
		SpringApplication.run(KlbApplication.class, args);
	}

}

