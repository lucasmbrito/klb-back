package br.com.klb.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.klb.model.Credito;
import br.com.klb.service.interfaces.ICreditoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/credito")
@CrossOrigin
@Api(value = "Credito controller", tags = { "credito-controller" })
public class CreditoController {
	
	@Autowired
	private ICreditoService creditoService;
	
	@GetMapping("/findAll")
	@ApiOperation(value = "Find all Client",response = Credito.class)
	public List<Credito> findAll(){
		return creditoService.listAll();
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation(value = "Remove Credito")
	public void remove(@PathVariable Long id) {
		creditoService.remove(id);
	}
	
	@RequestMapping(value= "/save", method = RequestMethod.POST)
	@ApiOperation(value = "Save Credito",response = String.class)
	public ResponseEntity<Credito> save(@Valid @RequestBody Credito credito) {
		return new ResponseEntity<Credito>(
				creditoService.save(credito) ,HttpStatus.CREATED
		);
	}

}
