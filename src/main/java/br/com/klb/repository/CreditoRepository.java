package br.com.klb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.klb.model.Credito;

@Repository
public interface CreditoRepository extends JpaRepository<Credito, Long> {

}
