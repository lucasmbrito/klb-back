package br.com.klb.service.interfaces;

import java.util.List;

import br.com.klb.model.Credito;

public interface ICreditoService {
	
	Credito save(Credito credito);

	void remove(Long id);
	
	List<Credito> listAll();

}
