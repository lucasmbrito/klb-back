package br.com.klb.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.klb.model.Credito;
import br.com.klb.repository.CreditoRepository;
import br.com.klb.service.interfaces.ICreditoService;

@Service
public class CreditoService implements ICreditoService{

	@Autowired
	private CreditoRepository creditoRepository;
	
	public List<Credito> listAll() {
		return this.creditoRepository.findAll();
	}

	public void remove(Long id) {
		this.creditoRepository.deleteById(id);
		
	}
	
	public Credito save(Credito credito) {
		if(credito.getTipoRisco().equals("B")) {
			credito.setTaxaJuros(new BigDecimal(10));
			BigDecimal juros = credito.getTaxaJuros().multiply(credito.getLimiteCredito()).divide(new BigDecimal(100));
			credito.setLimiteCredito(juros.add(credito.getLimiteCredito()));
		}else if(credito.getTipoRisco().equals("C")) {
			credito.setTaxaJuros(new BigDecimal(20));
			BigDecimal juros = credito.getTaxaJuros().multiply(credito.getLimiteCredito()).divide(new BigDecimal(100));
			credito.setLimiteCredito(juros.add(credito.getLimiteCredito()));
		} else {
			credito.setTaxaJuros(new BigDecimal(0));
		}
		return this.creditoRepository.save(credito);
	}
}
